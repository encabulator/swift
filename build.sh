#! /bin/bash

#   NAME
#
#       build.sh - a script to build FizzBuzz
#
#   SYNOPSYS
#
#       build.sh <target> <int | long> <src>
#
#       where
#           <target>        is the executable file to be created.
#           <int | long>    specifies the type of each Fibonacci number.
#           <src>           is the source code file.
#
#   DESCRIPTION
#
#       This script is just a thin wrapper over GCC, making normal compilation
#       of FizzBuzz just a little more convenient. The script might also be
#       extended in to order to create a portable API for supporting other compilers
#       or operating systems.
#
#   EXAMPLE
#
#       A "standard" build of FizzBuzz would look like this:
#
#       build.sh FizzBuzz int FizzBuzz.cpp
#
#   DEPENDENCIES
#
#       This script must be modified if new Fibonacci number types are added.
#
#   HISTORY
#
#       10/26/17: Created by Peter Dunn

if [ $# -lt 3 ]
then
    echo "build.sh: 3 arguments are required" >&2
    echo >&2
    echo Usage: >& 2
    echo "   build.sh <target> <int | long> <src>"
    exit 1
fi

if [ "$2" != "int" ] && [ "$2" != "long" ]
then
    echo "build.sh: An unsupported Fibonacci number type was specified \(\"$2\"\)." >&2
    echo "  The type must be \"int\" or \"long\"."
    exit 1
fi

if [ ! -f $3 ]
then
    echo "build.sh: A file named \"$3\" could not be found." >&2
    exit 1
fi

target=$1
fibType=$2
upperFibType=${fibType^^}
src=$3
gcc -O3 -o ${target} -DT_TOKEN=T_TOKEN_${upperFibType} ${src}
