/*
    NAME

        FizzBuzz

    SYNOPSIS

        FizzBuzz <sequence-count> [ origin ]

    DESCRIPTION

        FizzBuzz is a command line program which outputs a single line of text (to the
        standard output stream) for each of the first "sequence-count" numbers in the
        Fibonacci series.

        The line of text appearing for each Fibonacci number is defined by the following
        rules, in decreasing order of precedence:

        n is a prime number:    "BuzzFizz"
        n is a multiple of 15:  "FizzBuzz"
        n is a multiple of 3:   "Buzz"
        n is a multiple of 5:   "Fizz"
        none of the above:      the Fibonacci number itself

        The order of precedence is important because numbers can be members of more than one
        subset of the Fibonacci series. Most notably, 3 and 5 are multiples of 3 and 5,
        respectively, but since they are both members of the high-precedence set of prime
        numbers, they are both represented as "BuzzFizz" in the program's output.

        The first number in a Fibonacci series may be zero or one; i.e., it could begin with
        0, 1, 1, 2, or it could begin with 1, 1, 2. The optional "origin" argument specifies
        the first number, and therefore must be 0 or 1 when it is present. By default, the
        first number is 1.

    REQUIREMENTS

        Environments tested (with no required code modifications):
            1) Ubuntu Version 14.04, using GCC Version 4.8.4.
            2) Windows 7, using Visual Studio 2012.
                a) Disable precompiled headers in the project settings.
                b) Remove stadfx.h from the project.
                c) Disable warning 4996 ("unsafe" use of strcpy instead of strcpy_s).
            3) Cygwin under Windows 7, using GCC 4.5.3.

        There are no external dependencies.

    COMPILATION

        A simple command to compile FizzBuzz.cpp is:

            gcc -o FizzBuzz FizzBuzz.cpp

        This command could be used to generate a version of FizzBuzz in which Fibonacci
        numbers are represented as longs, and additional optimization is performed by the
        compiler:

            gcc -o FizzBuzzLong -O3 -DT_TOKEN=T_TOKEN_LONG FizzBuzz.cpp

        The script build.sh may also be used. Its command format is:

            build.sh <target> <int | long> <src>

        The build.sh invocation which is equivalent to the above GCC command is:

            build.sh FizzBuzzLong long FizzBuzz.cpp

    EXAMPLES

        The command "FizzBuzz 10" should produce the following output:

			1
			1
			BuzzFizz
			BuzzFizz
			BuzzFizz
			8
			BuzzFizz
			Buzz
			34
			Fizz

        The command "FizzBuzz 10 0" should produce this output:

			0
			1
			1
			BuzzFizz
			BuzzFizz
			BuzzFizz
			8
			BuzzFizz
			Buzz
			34

    HISTORY

            10/25/17:   Created by Peter Dunn.
*/

#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "limits.h"
#include "math.h"

        /*
            The following macros and typedefs supply crude support for polymorphic
            Fibonacci numbers.

            If other integer types are added, particular attention should be paid to
            the "encode" function below.
        */

#define T_TOKEN_INT     0       // A handle representing "int", allowing for #if tests.
#define T_TOKEN_LONG    1       // A handle representing "long". 

#ifndef T_TOKEN                         // Define the integer type token if it wasn't defined
    #define T_TOKEN     T_TOKEN_INT     // on the compilation command line.
#endif

        /*
            "T_Integer" will be used to denote the type of each Fibonacci number, and
            "INTEGER_MAX" will be used to denote the maximum value of each number.
        */

#if T_TOKEN==T_TOKEN_LONG
    typedef long                T_Integer;
    #define INTEGER_MAX         LONG_MAX
#else
    typedef int                 T_Integer;
    #define INTEGER_MAX         INT_MAX
#endif

        /*
            By default, the first Fibonacci number in our sequence will be 1.
        */

#define DEFAULT_ORIGIN 1

#ifndef MAX_LINE_LEN                // An arbitrary maximum length of a line in the output stream.
    #define MAX_LINE_LEN    100
#endif

#define MAX_DIAG_LEN    1024        // An arbitrary maximum length of a diagnostic.

        /*
           "emit" outputs a line associated with a Fibonacci number ("Fizz\n", "Buzz\n", etc.).
        */

void emit( const char *str )
{
    printf( "%s", str );
}

        /*
            "emitDiag" outputs a diagnostic string.
        */

void emitDiag( const char *diag )
{
    fprintf( stderr, "%s", diag );
}

        /*
            "usage" emits a diagnostic showing the format of the FizzBuzz command, when
            the command has been invoked with a poorly-formatted argument list.
        */

void usage( )
{
    emitDiag( "Usage: FizzBuzz <sequence length> [ origin ]\n" );
}

        /*
            "overflow" handles the case when the next Fibonacci number would be too large
            to be contained in the currently-used integer type. A diagnostic is issued,
            and the program exits.
        */

void overflow( int req, int cur )
{
    char    diag[ MAX_DIAG_LEN + 1 ];

    sprintf( diag, "FizzBuzz: The Fibonacci sequence was too long (%d elements); \
an overflow would occur on element %d.\n", req, cur );
    emitDiag( diag );
    exit( 1 );
}

        /*
            "encode" converts an integer value to a string. If the type token representing
            the Fibonacci number type isn't T_TOKEN_LONG, it's assumed to be T_TOKEN_INT.
        */

char *encode( char *encoded, T_Integer val )
{

#if T_TOKEN==T_TOKEN_LONG
    sprintf( encoded, "%ld", val );
#else
    sprintf( encoded, "%d", val );
#endif
    return encoded;
}

        /*
            "decodeInteger" attempts to decode a string to an integer. An unsuccessful conversion
            is treated as a fatal error, and the name specified for the integer value is
            included in the generated diagnostic.
        */

int decodeInteger( const char *name, char *str )
{
    char    diag[ MAX_DIAG_LEN + 1 ];
    int     val;

    if( sscanf( str, "%d", &val) < 1 )
    {
        sprintf( diag, "FizzBuzz: Error: The %s (\"%s\") was not a valid integer.\n", name, str );
        emitDiag( diag );
        exit( 1 );
    }
    return val;
}

        /*
            "decodeSequenceLength" converts the command line argument containing the sequence length
            to an integer. The program exits if the argument is not a valid, non-negative integer.
        */

int decodeSequenceLength( char *str )
{
    char    diag[ MAX_DIAG_LEN + 1 ];

    int len = decodeInteger( "sequence length", str );
    if( len < 0 )
    {
        sprintf( diag, "FizzBuzz: Error: The sequence length (%d) was less than zero.\n", len );
        emitDiag( diag );
        exit( 1 );
    }
    return len;
}

        /*
            "decodeOrigin" converts the command line argument containing the sequence origin to an
            integer. The program exits if the argument isn't 0 or 1.
        */

int decodeOrigin( char *str )
{
    char    diag[ MAX_DIAG_LEN + 1 ];
    int     origin = decodeInteger( "origin", str );

    if( origin < 0  ||  origin > 1 )
    {
        sprintf( diag, "FizzBuzz: Error: The origin (specified as %d) must be 0 or 1.\n", origin );
        emitDiag( diag );
        exit( 1 );
    }
    return origin;
}

        /*
            "isPrime" determines whether the specified integer is a prime number.
            The integer is successively divided by an increasing denominator,
            starting with 2. If a denominator is found which produces a remainder
            of zero, the integer is NOT prime. The search fails when the square of
            the denominator becomes larger than the integer.
        */

bool isPrime( T_Integer i )
{
    if( i < 2 )
        return false;
    for ( T_Integer denom = 2; denom * denom <= i; denom++ )
        if( i % denom == 0 )
            return false;
    return true;
}

        /*
            "fibonacciToKeyword" determines the FizzBuzz "keyword" associated with
            the specified Fibonacci number. When a Fibonacci number is smaller than 2,
            is neither a multiple of 3 or 5, and is also not a prime number, no such
            keyword exists, so an empty string is returned. (The empty string indicates
            that the Fibonacci number itself will be output, instead of a keyword.)
        */

const char *fibonacciToKeyword( T_Integer fib )
{
    if( fib < 2 )
        return "";
    if( fib % 15 == 0 )
        return "FizzBuzz";
    if( isPrime( fib ) )
        return "BuzzFizz";
    if( fib % 3 == 0 )
        return "Buzz";
    if( fib % 5 == 0 )
        return "Fizz";
    return "";
}

        /*
            "fibonacciToLine" constructs an output line for a Fibonacci number.
            If the number qualifies for a keyword (like "Fizz"), we use that keyword,
            copying it to the output buffer. Otherwise, the number is encoded to a string in
            the output buffer. In both cases, a newline is appended, and the function
            returns "buf".
        */

const char *fibonacciToLine( char *buf, T_Integer fib )
{
    const char    *keyword = fibonacciToKeyword( fib );

    strlen( keyword ) > 0  ?  strcpy( buf, keyword )  :  encode( buf, fib );
    return( strcat( buf, "\n" ) );
}

        /*
            "processFibonacciSequence" contains FizzBuzz's main loop, iterating through
            the Fibonacci series until the requested number of elements have been
            processed, or an overflow of the current integral data type would occur.

            The first two Fibonacci numbers, contained in "initial", are treated as special
            cases.

            Starting with the third Fibonacci number...
                Repeat until one of the two termination cases mentioned above occurs:
                    Would the calculation of the next Fibonacci number cause an overflow?
                    If not, perform the addition, and set the new previous and current values.
                    Emit the updated current Fibonacci number.
        */

void processFibonacciSequence( int len, int origin )
{
    char        buf[ MAX_LINE_LEN + 1 ];
    T_Integer   initial[ ] = { origin, 1 };
    T_Integer   prev = *initial;
    T_Integer   cur = *( initial + 1 );
    T_Integer   new_cur;

    for( int i = 0; i < 2  &&  i < len; i++ )
        emit( fibonacciToLine( buf, *( initial + i ) ) );
    for( int j = 3; j <= len; j++ )
    {
        if( cur > INTEGER_MAX - prev )
            overflow( len, j );
        new_cur = prev + cur;
        prev = cur;
        cur = new_cur;
        emit( fibonacciToLine( buf, cur ) );
    }
}

        /*
            "decodeArgs" sets the sequence length and possibly the origin (0 or 1) from
            the command-line arguments.
        */

void decodeArgs( int *len, int* origin, int argc, char *argv[ ] )
{
    if( argc >= 2 )
        *len = decodeSequenceLength( argv[ 1 ] );
    if( argc >= 3 )
        *origin = decodeOrigin( argv[ 2 ] );
}

        /*
            "processArgs" looks at the command line arguments and attempts to set the sequence
            length, and possibly the sequence origin. An "argc" value less than 2 indicates that
            the sequence length argument, which is required, was not specified; this is treated
            as a fatal error.  An "argc" value of 2 or more means that at least the sequence
            length has been specified. A value larger than 3 means that superfluous arguments
            were found; this is reported as a warning, and the program continues.
        */

void processArgs( int *len, int *origin, int argc, char *argv[ ] )
{
    switch( argc )
    {
        case 0:
        case 1:
            emitDiag( "FizzBuzz: Error: The length of the Fibonacci sequence was not specified.\n" );
            usage( );
            exit( 1 );

        default:
            decodeArgs( len, origin, argc, argv );
            if( argc > 3 )
            {
                emitDiag( "FizzBuzz: Warning: One or more extra arguments were present, but ignored.\n" );
                usage( );
            }
            break;
   }
}

        /*
            "main" processes the command line arguments, looking for the required sequence length,
            and the optional sequence origin (0 or 1). After successful argument processing, the
            requested sequence itself is processed.
        */

int main( int argc, char *argv[ ] )
{
    int         len;
    int         origin = DEFAULT_ORIGIN;

    processArgs( &len, &origin, argc, argv );
    processFibonacciSequence( len, origin );
    return 0;
}
